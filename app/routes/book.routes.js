module.exports = app => {
  const book = require("../controllers/book.controller.js");

  // Create a new book
  app.post("/livros", book.create);

  // Read all book
  app.get("/livros", book.findAll);

  // Read a single book with id
  app.get("/livros/:id", book.findOne);

  // Update a book with id
  app.put("/livros/:id", book.update);

  // Delete a book with id
  app.delete("/livros/:id", book.delete);

};
