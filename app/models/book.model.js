const sql = require("./db.js");

// constructor
const Book = function(book) {
  this.titulo = book.titulo;
  this.autor = book.autor;
  this.sinopse = book.sinopse;
  this.data_publicacao = book.data_publicacao;
  this.edicao = book.edicao;
  this.editora = book.editora;
  this.idioma = book.idioma;
  this.capa_url = book.capa_url;
};

Book.create = (newBook, result) => {
  sql.query("INSERT INTO livros SET ?", newBook, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("Book created.");
    result(null, { id: res.insertId, ...newBook });
  });
};

Book.findById = (id, result) => {
  sql.query(`SELECT * FROM livros WHERE id = ${id}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("Book found.");
      result(null, res[0]);
      return;
    }

    // not found book with the id
    result({ kind: "not_found" }, null);
  });
};

Book.getAll = result => {
  sql.query("SELECT * FROM livros", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("livros: ", res);
    result(null, res);
  });
};

Book.updateById = (id, book, result) => {
  sql.query(
    "UPDATE livros SET titulo = ?, autor = ?, sinopse = ?, data_publicacao = ?, edicao = ?, editora = ?, idioma = ?, capa_url = ? WHERE id = ?",
    [book.titulo, book.autor, book.sinopse, book.data_publicacao, book.edicao, book.editora, book.idioma, book.capa_url,  id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found book with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("Book updated.");
      result(null, { id: id, ...book });
    }
  );
};

Book.remove = (id, result) => {
  sql.query("DELETE FROM livros WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found book with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("Book deleted.");
    result(null, res);
  });
};

module.exports = Book;
