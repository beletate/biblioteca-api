const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors')

const app = express();

app.use(express.static('./public'));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Configure cors 
app.set('secret', 'aktienow');
const corsOptions = {
  exposedHeaders: ['x-access-token']
};
app.use(cors(corsOptions));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome." });
});

require("./app/routes/book.routes.js")(app);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
